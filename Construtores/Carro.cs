﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Construtores
{
    public class Carro
    {
        private string marca;

        private string modelo;

        private string cor;

        public string Marca { get; set; }

        public string Modelo { get; set; }

        public string Cor { get; set; }

  
        public Carro():this("Fiat", "Putnto", "Vermelho") { }

     
        public Carro(Carro carro): this(carro.Marca, carro.Modelo, carro.Cor) { }
        public Carro(string marca, string modelo, string cor)
        {
            Marca = marca;
            Modelo = modelo;
            Cor = cor;
        }

        public Carro Clone(Carro carro)
        {
            Marca = carro.Marca;
            Modelo = carro.Modelo;
            Cor = carro.Cor;
            return this;
        }
    }
}
