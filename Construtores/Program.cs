﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Construtores
{
    class Program
    {
        static void Main(string[] args) 
        {
            Carro bolinhas = new Carro();

            Carro popo = new Carro();
            popo.Clone(bolinhas);

            Carro boguinhas = new Carro("Renault", "Megane", "Amarelo");
            Console.ReadKey();
        }
    }
}
